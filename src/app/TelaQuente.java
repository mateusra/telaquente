package app;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @Instrutor: Isabel C. M. Rosseti
 * @Autor: Mateus Rodrigues Alves
 * @ProjetoComputacional: Tela Quente
 */
public class TelaQuente {

    /**
     * Inicia a aplicação
     *
     * @param args
     */
    public static void main(String[] args) {
        int numeroDaPergunta = 0;
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-") && opcoesValidas(i, args, ++numeroDaPergunta)) {
                executaOperacoes(i, args, numeroDaPergunta);
            }
        }
        if (numeroDaPergunta == 0 && args.length > 0) {
            System.err.println("Parece que os parametros passados não corresponderam as esperados. As perguntas possiveis são:\n"
                    + "<-generos>, <-filmes> e <-episodios>. Um exemplo de modelo esperado, já agregado ao comando de terminal, é:\n"
                    + "java TelaQuente.jar -generos Megan_Fox");
        } else if (numeroDaPergunta == 0) {
            System.err.println("Nenhuma pergunta foi respondida, pois não foi feita nenhuma.");
        }
    }

    /**
     * Checa se os parâmetros são válidos
     *
     * @param posicao
     * @param parametos
     * @param numeroDaPergunta
     * @return valido
     */
    private static boolean opcoesValidas(int posicao, String[] parametos, int numeroDaPergunta) {
        String modeloDePergunta = "";
        try {
            switch (parametos[posicao]) {
                case "-generos":
                    modeloDePergunta = "<-generos> <nome_do_ator>";
                    if (parametos[posicao + 1].matches(".*(-filme|-episodio|-genero).*")) {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                    break;
                case "-episodios":
                    modeloDePergunta = "<-episodios> <nome_do_ator> <nome_da_serie>";
                    if (parametos[posicao + 1].matches(".*(-filme|-episodio|-genero).*") || parametos[posicao + 2].matches(".*(-filme|-episodio|-genero).*")) {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                    break;
                case "-filmes":
                    modeloDePergunta = "<-filmes> <nome_do_ator1> <nome_da_ator2> <decada>";
                    if (parametos[posicao + 1].matches(".*(-filme|-episodio|-genero).*") || parametos[posicao + 2].matches(".*(-filme|-episodio|-genero).*") || !parametos[posicao + 3].matches("\\d+")) {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                    break;
                default:
                    System.err.println("A opção <" + parametos[posicao] + "> é inválida, verefique a " + numeroDaPergunta + "ª pergunta.\n"
                            + "As possibilidades de parâmetros corretos são: <-generos>, <-episodios> ou <-filmes>\n");
                    return false;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Não foi possível responder a " + numeroDaPergunta + "ª pergunta."
                    + "\nPois estão faltando um ou mais subparametros, verifique se a sua pergunta se encaixa no modelo: " + modeloDePergunta +"\n");
            return false;
        }
        return true;
    }

    /**
     * Responde as perguntas feitas pelo usuário através dos args
     *
     * @param posicao
     * @param parametros
     * @param numeroDaPergunta
     */
    private static void executaOperacoes(int posicao, String[] parametros, int numeroDaPergunta) {
        Object[] material = null;
        String nomeDoArquivo = parametros[posicao].substring(1) + ".txt";;
        String modeloDePergunta = "";
        try {
            switch (parametros[posicao]) {
                case "-generos":
                    modeloDePergunta = "<-generos> <nome_do_ator>";
                    material = cinema.Operacoes.generosDeFilmes(parametros[posicao + 1]);
                    break;
                case "-episodios":
                    modeloDePergunta = "<-episodios> <nome_do_ator> <nome_da_serie>";
                    material = cinema.Operacoes.autoRepresentacao(parametros[posicao + 1], parametros[posicao + 2]);
                    break;
                case "-filmes":
                    modeloDePergunta = "<-filmes> <nome_do_ator1> <nome_da_ator2> <decada>";
                    material = cinema.Operacoes.filmesEmComum(parametros[posicao + 1], parametros[posicao + 2], Integer.parseInt(parametros[posicao + 3]));
                    break;
            }
        } catch (MalformedURLException e) {//Exceção para caso algum parâmetro não de resultado ou haja algum problema no momento da requisição
            System.err.println("Não foi possível realizar a busca para responder a " + numeroDaPergunta + "ª pergunta,"
                    + " pois um ou mais parâmetros não retornaram resultado."
                    + "\nVerefique se os parâmetros se encaixam no modelo " + modeloDePergunta +"\n");
        } catch (IOException e) {//Exceção para caso haja um problema com a resposta da API
            System.err.println("Não foi possível realizar a busca para responder a " + numeroDaPergunta + "ª pergunta."
                    + "\nVerefique sua conexão com a Internet, caso o erro persista tente mais tarde\n");
        } catch (RuntimeException e) {//Exceção para caso ocorra alguma anomalia na reposta da API(ex: "air_date"=null)
            System.err.println("Não foi possível responder a " + numeroDaPergunta + "ª pergunta."
                    + "\nPois um ou mais parâmetros retornaram dados inválidos\n");
        }
        escreveArquivo(nomeDoArquivo, material, numeroDaPergunta);
    }

    /**
     * Escreve a resposta no arquivo
     *
     * @param nomeDoArquivo
     * @param material
     * @param numeroDaPergunta
     */
    private static void escreveArquivo(String nomeDoArquivo, Object[] material, int numeroDaPergunta) {
        try (BufferedWriter escritor = new BufferedWriter(new FileWriter(nomeDoArquivo))) {
            if (material != null) {
                for (Object materialAtual : material) {
                    escritor.write(materialAtual.toString());
                    escritor.newLine();
                }
            }
        } catch (IOException e) {
            System.err.println("Não foi possível criar o arquivo de resposta para a " + numeroDaPergunta + "ª pergunta.\n"
                    + "Verifique se há espaço sobrando ou se a pasta da aplicação está protegida contra gravação em disco e tente novamente\n");
        }
    }
}
