package cinema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representa de um filme, com seus respectivos personagens, título e ano de
 * lançamento
 */
public class Filme implements Producao {

    private int id;
    private String titulo;
    private int ano;
    private List<String> generos;
    private List<Personagem> elenco;

    /**
     * Cria um filme
     *
     * @param id
     * @throws IOException
     * @throws RuntimeException
     */
    Filme(int id) throws IOException, RuntimeException {
        this.id = id;

        //Realisando a requisição para a API para descobrir o ano de laçamento e o título do filme
        URL requisicao = new URL("http://api.themoviedb.org/3/movie/" + id + "?api_key=f3c5689648013f06492813f7304d91e9");
        StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar o ano e o título
        Matcher regex = Pattern.compile("(?<=\")(\\d+)").matcher(resultado);
        if (regex.find()) {
            this.ano = Integer.parseInt(regex.group());
        } else {
            this.ano = 0;
        }

        regex = Pattern.compile("(?<=\"title\":\")(.+?)(?=\",\")").matcher(resultado);
        regex.find();
        this.titulo = regex.group();

        //Processando a resposta da API com Regex para encontrar os gêneros nos quais esse filme foi classificado
        regex = Pattern.compile("(?<=\\[)([^\\]]*)").matcher(resultado);
        regex.find();
        resultado = new StringBuilder(regex.group());
        regex = Pattern.compile("(?<=name\":\")([^\"]*)").matcher(resultado);
        this.generos = new ArrayList<>();
        while (regex.find()) {
            this.generos.add(regex.group(1));
        }

        //Realisando a requisicao para a API para descobrir o elenco do filme
        requisicao = new URL("http://api.themoviedb.org/3/movie/" + id + "/credits?api_key=f3c5689648013f06492813f7304d91e9");
        resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());
        List<Personagem> elencoTemp = new ArrayList<>();

        //Processando a resposta da API com Regex para encontrar os id e os nome do personagens que participaram do filme
        regex = Pattern.compile("(?<=.\\{\"id\":)(\\d+)(?:,\"name\":\")(.+?)(?:\",\"character\":[\"|n])(.*?)(?=[\"|l],\")").matcher(resultado);
        this.elenco = new ArrayList<>();
        while (regex.find()) {
            Ator atorAtual = null;
            int idAtor = Integer.parseInt(regex.group(1));
            if (Colecoes.findAtor(idAtor) == null) {
                atorAtual = new Ator(idAtor);
                Colecoes.addAtor(atorAtual);
            } else {
                atorAtual = Colecoes.findAtor(idAtor);
            }
            this.elenco.add(new Personagem(((regex.group(3).toLowerCase()).matches("Herself||Himself||ul||")) ? regex.group(2) : regex.group(3), atorAtual));
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitulo() {
        return titulo;
    }

    @Override
    public int getAno() {
        return ano;
    }

    /**
     * Retorna os gêneros do filme
     *
     * @return gêneros
     */
    public String[] getGeneros() {
        return generos.toArray(new String[generos.size()]);
    }

    /**
     * Compara dois filmes, se forem iguais retorna verdadeiro
     *
     * @param cover
     * @return sãoIguais
     */
    @Override
    public boolean equals(Object cover) {
        if (cover instanceof Filme) {
            return this.id == ((Filme) cover).getId();
        }
        return false;
    }

    /**
     * Retorna um personagem do filme dado um índice de 0 até, exclusivamente, o
     * valor retornado pelo tamanhoDoElenco()
     *
     * @param indice
     * @return enesimoPersonagem
     */
    public Personagem getPersonagem(int indice) {
        return elenco.get(indice);
    }

    /**
     * Retorna um número de personagens do filme
     *
     * @return númeroDePersonagens
     */
    public int tamanhoDoElenco() {
        return elenco.size();
    }

    @Override
    public String toString() {
        return titulo;
    }
}
