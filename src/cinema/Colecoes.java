package cinema;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa uma biblioteca de uma filmes, series e atores
 */
class Colecoes {

    private static final List<Ator> atores = new ArrayList<>();
    private static final List<Filme> filmes = new ArrayList<>();
    private static final List<Serie> series = new ArrayList<>();

    /**
     * Adiciona um ator na coleção
     *
     * @param ator
     */
    public static void addAtor(Ator ator) {
        if (!atores.contains(ator)) {
            atores.add(ator);
        }
    }

    /**
     * Adiciona um filme na coleção
     *
     * @param filme
     */
    public static void addFilme(Filme filme) {
        if (!filmes.contains(filme)) {
            filmes.add(filme);
        }
    }

    /**
     * Adiciona uma serie na coleção
     *
     * @param serie
     */
    public static void addSerie(Serie serie) {
        if (!series.contains(serie)) {
            series.add(serie);
        }
    }

    /**
     * Retorna um ator da coleção caso exista, senão retorna nulo
     *
     * @param id
     * @return ator
     */
    public static Ator findAtor(int id) {
        for (Ator ator : atores) {
            if (ator.getId() == id) {
                return ator;
            }
        }
        return null;
    }

    /**
     * Retorna um ator da coleção caso exista, senão retorna nulo
     *
     * @param nome
     * @return ator
     */
    public static Ator findAtor(String nome) {
        for (Ator ator : atores) {
            if (ator.getNome().equalsIgnoreCase(nome)) {
                return ator;
            }
        }
        return null;
    }

    /**
     * Retorna um filme da coleção caso exista, senão retorna nulo
     *
     * @param id
     * @return filme
     */
    public static Filme findFilme(int id) {
        for (Filme filme : filmes) {
            if (filme.getId() == id) {
                return filme;
            }
        }
        return null;
    }

    /**
     * Retorna um filme da coleção caso exista, senão retorna nulo
     *
     * @param nome
     * @return filme
     */
    public static Filme findFilme(String nome) {
        for (Filme filme : filmes) {
            if (filme.getTitulo().equalsIgnoreCase(nome)) {
                return filme;
            }
        }
        return null;
    }

    /**
     * Retorna uma serie da coleção caso exista, senão retorna nulo
     *
     * @param id
     * @return série
     */
    public static Serie findSerie(int id) {
        for (Serie serie : series) {
            if (serie.getId() == id) {
                return serie;
            }
        }
        return null;
    }

    /**
     * Retorna uma serie da coleção caso exista, senão retorna nulo
     *
     * @param nome
     * @return série
     */
    public static Serie findSerie(String nome) {
        for (Serie serie : series) {
            if (serie.getTitulo().equalsIgnoreCase(nome)) {
                return serie;
            }
        }
        return null;
    }
}
