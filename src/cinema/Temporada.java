package cinema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representa uma temporada de uma serie, com seus respectivos episódios, numero
 * e ano de lançamento
 */
public class Temporada {

    private int numero;
    private int ano;
    private Serie serie;
    private List<Episodio> episodios;

    /**
     * Cria uma temporada de uma série
     *
     * @param id
     * @throws IOException
     * @throws RuntimeException
     */
    Temporada(int numero, Serie serie) throws IOException, RuntimeException {
        this.numero = numero;
        this.serie = serie;

        //Realizando a requisição para a API para descobrir a data e os episódios da temporada
        URL busca = new URL("http://api.themoviedb.org/3/tv/" + serie.getId() + "/season/" + numero + "?api_key=f3c5689648013f06492813f7304d91e9");
        StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(busca.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar a data e o número de episódios da temporada
        Matcher regex = Pattern.compile("(\\d+)-").matcher(resultado);
        if (regex.find()) {
            this.ano = Integer.parseInt(regex.group(1));
        } else {
            this.ano = 0;
        }
        regex = Pattern.compile("(\\d+)(?!.+e_number\":)").matcher(resultado);
        int numEpisodios = 0;
        if (regex.find()) {
            numEpisodios = Integer.parseInt(regex.group());
        }
        this.episodios = new ArrayList<>();
        for (int i = 1; i <= numEpisodios; i++) {
            this.episodios.add(new Episodio(serie.getId(), i, this));
        }
    }

    /**
     * Retorna o número da temporada
     *
     * @return numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Retorna o ano de inicio da temporada
     *
     * @return anoDeInicio
     */
    public int getAno() {
        return ano;
    }

    /**
     * Retorna um episódio da temporada dado um incide de 0 até, exclusivamente,
     * o valor retornado pelo tamanho()
     *
     * @param indice
     * @return enesimoEpisódio
     */
    public Episodio getEpisodio(int indice) {
        return episodios.get(indice);
    }

    /**
     * Retorna o número de episódios
     *
     * @return númeroDeEpisódios
     */
    public int tamanho() {
        return episodios.size();
    }

    @Override
    public String toString() {
        return "Temporada:" + ((numero < 10) ? "0" + numero : numero) + "\tNúmero de Episódios:" + ((this.tamanho() < 10) ? "0" + this.tamanho() : this.tamanho());
    }
}
