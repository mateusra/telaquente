package cinema;

/**
 * @Instrutor: Isabel C. M. Rosseti
 * @Autor: Mateus Rodrigues Alves
 * @ProjetoComputacional: Tela Quente
 */
public interface Producao {

    /**
     * Retorna o id da produção
     *
     * @return id
     */
    public abstract int getId();

    /**
     * Retorna o título da produção
     *
     * @return título
     */
    public abstract String getTitulo();

    /**
     * Retorna o ano de lançamento da produção
     *
     * @return anoDeLançamento
     */
    public abstract int getAno();

}
