package cinema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representa um ator, com seu respectivo id e nome
 */
public class Ator {

    private int id;
    private String nome;
    
    /**
     * Cria um ator
     *
     * @param id 
     * @throws IOException
     * @throws RuntimeException
     */
    Ator(int id) throws IOException, RuntimeException {
        this.id = id;

        //Realisando a requisicao para a API para descobrir o nome do ator
        URL requisicao = new URL("http://api.themoviedb.org/3/person/" + id + "?api_key=f3c5689648013f06492813f7304d91e9");
        StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar o nome do ator
        Matcher regex = Pattern.compile("(?<=me\":\")(.+?)(?=\",\")").matcher(resultado);
        regex.find();
        this.nome = regex.group();
    }

    /**
     * Retorna o id do ator
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Retorna o nome do ator
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "id: " + id + "\tNome: " + nome;
    }

    /**
     * Compara dois atores, se forem iguais retorna verdadeiro
     *
     * @param cover
     * @return sãoIguais
     */
    @Override
    public boolean equals(Object cover) {
        if (cover instanceof Ator) {
            return this.id == ((Ator) cover).getId();
        }
        return false;
    }

}
