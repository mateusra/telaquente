package cinema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representa uma série, com suas respectivas temporadas, título e ano de
 * lançamento
 */
public class Serie implements Producao {

    private int id;
    private String titulo;
    private int ano;
    private List<Temporada> temporadas;

    /**
     * Cria uma série
     *
     * @param id
     * @throws IOException
     * @throws RuntimeException
     */
    Serie(int id) throws IOException, RuntimeException {
        this.id = id;

        //Realisando a requisição para a API para descobrir a data e o título da série
        URL requisicao = new URL("http://api.themoviedb.org/3/tv/" + id + "?api_key=f3c5689648013f06492813f7304d91e9");
        StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar a data, o título e o número de temporadas da série
        Matcher regex = Pattern.compile("(?<=me\":\")(((?!\",\").)*)(?=\",\"n)").matcher(resultado);
        if (regex.find()) {
            this.titulo = regex.group();
        } else {
            this.titulo = "Não encontrado";
        }
        regex = Pattern.compile("(\\d+)-(?!.+\"season_number\":0)").matcher(resultado);
        regex.find();
        this.ano = Integer.parseInt(regex.group(1));
        regex = Pattern.compile("(\\d+)(?=\\}\\])").matcher(resultado);
        temporadas = new ArrayList<>();
        regex.find();
        int numTemporadas = Integer.parseInt(regex.group());
        for (int i = 1; i <= numTemporadas; i++) {
            temporadas.add(new Temporada(i, this));
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitulo() {
        return titulo;
    }

    @Override
    public int getAno() {
        return ano;
    }

    /**
     * Retorna uma temporada da serie dado um indice de 0 ate, exclusivamente, o
     * valor retornado pelo tamanho()
     *
     * @param indice
     * @return enesimaTemporada
     */
    public Temporada getTemporada(int indice) {
        return temporadas.get(indice);
    }

    /**
     * Retorna o numero de temporadas da serie
     *
     * @return numeroDeTempordas
     */
    public int tamanho() {
        return temporadas.size();
    }

    /**
     * Compara duas series, se forem iguais retorna verdadeiro
     *
     * @param cover
     * @return sãoIguais
     */
    @Override
    public boolean equals(Object cover) {
        if (cover instanceof Serie) {
            return this.id == ((Serie) cover).getId();
        }
        return false;
    }

    @Override
    public String toString() {
        return "Título: " + titulo + "\tAno de Lançamento: " + ano;
    }
}
