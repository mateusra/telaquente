package cinema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contém as operações para responder a determinadas perguntas
 */
public class Operacoes {

    /**
     * *
     * Descobre, dados um ator e uma série, os episódios dessa série no qual ele
     * se auto-representou.
     *
     * @param nomeAtor
     * @param nomeSerie
     * @return episodios
     * @throws IOException
     * @throws MalformedURLException
     */
    public static Episodio[] autoRepresentacao(String nomeAtor, String nomeSerie) throws IOException, MalformedURLException {

        //Tratando o parâmetro ator
        nomeAtor = nomeAtor.replaceAll("_", " ");
        Personagem atorPerso = new Personagem(nomeAtor, (Ator) criaArte(Ator.class, nomeAtor));

        //Tratando o parâmetro série
        nomeSerie = nomeSerie.replaceAll("_", " ");
        Serie serieAtual = (Serie) criaArte(Serie.class, nomeSerie);

        //Realizando busca através da serie para descobrir quais episódios o ator em questão se auto-representou 
        List<Episodio> resp = new ArrayList<>();
        for (int i = 0; i < serieAtual.tamanho(); i++) {
            Temporada temporadaAtual = serieAtual.getTemporada(i);
            for (int j = 0; j < temporadaAtual.tamanho(); j++) {
                Episodio episodioAtual = temporadaAtual.getEpisodio(j);
                for (int k = 0; k < episodioAtual.tamanhoDoElenco(); k++) {
                    Personagem personagemAtual = episodioAtual.getPersonagem(k);
                    if (personagemAtual.equals(atorPerso)) {
                        resp.add(episodioAtual);
                        break;
                    }
                }
            }
        }
        return resp.toArray(new Episodio[resp.size()]);
    }

    /**
     * Descobre os filmes em que dois atores trabalharam juntos em determinada
     * década
     *
     * @param nomeAtor1
     * @param nomeAtor2
     * @param decada
     * @return filmes
     * @throws java.net.MalformedURLException
     * @throws IOException
     */
    public static Filme[] filmesEmComum(String nomeAtor1, String nomeAtor2, int decada) throws MalformedURLException, IOException, RuntimeException {

        //Tratando o parâmetro nomeAtor1
        nomeAtor1 = nomeAtor1.replaceAll("_", " ");
        Ator atorAtual1 = (Ator) criaArte(Ator.class, nomeAtor1);

        //Tratando o parâmetro nomeAtor2
        nomeAtor2 = nomeAtor2.replaceAll("_", " ");
        Ator atorAtual2 = (Ator) criaArte(Ator.class, nomeAtor2);
        
        //Realizando a requisição para a API para descobrir os filmes nos quais o ator1 participou pelo id deste
        URL requisicao = new URL("http://api.themoviedb.org/3/person/" + atorAtual1.getId() + "/movie_credits?api_key=f3c5689648013f06492813f7304d91e9&query");
        StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar os ids e as datas de lançamentos dos filmes
        Matcher regex = Pattern.compile("(?<=:)(\\d+)(?:.+?)(?<=\\d{2})(\\d)(?=\\d-)(?=.+\"cre)").matcher(resultado);
        List<Filme> FilmesAtor1 = new ArrayList<>();
        while (regex.find()) {

            //Filtrando os filmes pela década, para evitar criar filmes desnecessários 
            if (regex.group(2).matches("\\d+") && (Integer.parseInt(regex.group(2)) * 10 == decada)) {
                FilmesAtor1.add((Filme) criaArte(Filme.class, regex.group(1)));
            }
        }

        //Realizando a requisição para a API para descobrir os filmes nos quais o ator2 participou pelo id deste
        requisicao = new URL("http://api.themoviedb.org/3/person/" + atorAtual2.getId() + "/movie_credits?api_key=f3c5689648013f06492813f7304d91e9&query");
        resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar os Ids e as datas de lancamentos dos filmes
        regex = Pattern.compile("(?<=:)(\\d+)(?:.+?)(?<=\\d{2})(\\d)(?=\\d-)(?=.+\"cre)").matcher(resultado);
        List<Filme> FilmesAtor2 = new ArrayList<>();
        while (regex.find()) {
            //Filtrando os filmes pela década, para evitar criar filmes desnecessários 
            if (regex.group(2).matches("\\d+") && (Integer.parseInt(regex.group(2)) * 10 == decada)) {
                FilmesAtor2.add((Filme) criaArte(Filme.class, regex.group(1)));
            }
        }
        FilmesAtor1.retainAll(FilmesAtor2);
        return FilmesAtor1.toArray(new Filme[FilmesAtor1.size()]);
    }

    /**
     * Descobre os gêneros de filmes nos quais um ator já nomeAtor em sua
     * carreira
     *
     * @param nomeAtor
     * @return gêneros
     * @throws java.net.MalformedURLException
     * @throws IOException
     */
    public static String[] generosDeFilmes(String nomeAtor) throws MalformedURLException, IOException, RuntimeException {

        //Tratando o parâmetro NomeAtor
        nomeAtor = nomeAtor.replaceAll("_", " ");
        Ator atorAtual = (Ator) criaArte(Ator.class, nomeAtor);

        //Realizando a requisição para a API para descobrir os filmes nos quais o ator em questão participou pelo id deste
        URL requisicao = new URL("http://api.themoviedb.org/3/person/" + atorAtual.getId() + "/movie_credits?api_key=f3c5689648013f06492813f7304d91e9&query");
        StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());

        //Processando a resposta da API com Regex para encontrar os ids dos filmes nos quais o ator em questão participou
        Matcher regex = Pattern.compile("(?<=d\":)(\\d+)(?=.+\\],\"cre)").matcher(resultado);
        List<String> generosDoAtor = new ArrayList<>();
        while (regex.find()) {
            Filme filmeAtual = (Filme) criaArte(Filme.class, regex.group());

            //Realizando busca pelos filmes para descobrir os gêneros em que o ator já atuou durante a sua carreira
            for (String genero : filmeAtual.getGeneros()) {
                if (!generosDoAtor.contains(genero)) {
                    generosDoAtor.add(genero);
                }
            }
        }
        return generosDoAtor.toArray(new String[generosDoAtor.size()]);
    }

    /**
     * Método auxiliar para criar filmes, séries e atores
     *
     * @param classe
     * @param param
     * @return obraDeArte
     * @throws IOException
     * @throws RuntimeException
     */
    private static Object criaArte(Class classe, String param) throws RuntimeException, IOException {
        Object retorno = null;
        switch (classe.getSimpleName()) {
            case "Filme":
                if (Colecoes.findFilme(Integer.parseInt(param)) == null) {
                    retorno = new Filme(Integer.parseInt(param));
                    Colecoes.addFilme((Filme) retorno);
                } else {
                    retorno = Colecoes.findFilme(Integer.parseInt(param));
                }
                break;
            case "Serie":
                if (Colecoes.findSerie(param) == null) {
                    URL requisicao = new URL("http://api.themoviedb.org/3/search/tv?api_key=f3c5689648013f06492813f7304d91e9&query=" + param.replaceAll(" ", "%20"));
                    StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());
                    Matcher regex = Pattern.compile("(?<=ts\":)(\\d+)").matcher(resultado);
                    regex.find();
                    int numDeResultados = Integer.parseInt(regex.group());
                    if (numDeResultados == 0) {
                        throw new MalformedURLException();
                    }
                    regex = Pattern.compile("(?<=d\":)(\\d+)").matcher(resultado);
                    regex.find();
                    int idSerie = Integer.parseInt(regex.group());
                    retorno = new Serie(idSerie);
                    Colecoes.addSerie((Serie) retorno);
                } else {
                    retorno = Colecoes.findSerie(param);
                }
                break;
            case "Ator":
                if (Colecoes.findAtor(param) == null) {
                    URL requisicao = new URL("http://api.themoviedb.org/3/search/person?api_key=f3c5689648013f06492813f7304d91e9&query=" + param.replaceAll(" ", "%20"));
                    StringBuilder resultado = new StringBuilder(new BufferedReader(new InputStreamReader(requisicao.openStream())).readLine());
                    Matcher regex = Pattern.compile("(?<=ts\":)(\\d+)").matcher(resultado);
                    regex.find();
                    int numDeResultados = Integer.parseInt(regex.group());
                    if (numDeResultados == 0) {
                        throw new MalformedURLException();
                    }
                    regex = Pattern.compile("(?<=d\":)(\\d+)").matcher(resultado);
                    regex.find();
                    int idAtor = Integer.parseInt(regex.group());
                    retorno = new Ator(idAtor);
                    Colecoes.addAtor((Ator) retorno);
                } else {
                    retorno = Colecoes.findAtor(param);
                }
                break;
        }
        return retorno;
    }
}
