package cinema;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representa um episódio de uma temporada de uma serie, com seus respectivos
 * personagens, titulo, numero e data de lançamento
 *
 */
public class Episodio {

    private int numero;
    private String titulo;
    private Date data;
    private Temporada temporada;
    private List<Personagem> elenco;

    /**
     * Cria um episódio de uma série
     *
     * @param id
     * @throws IOException
     * @throws RuntimeException
     */
    Episodio(int idSerie, int numero, Temporada temporada) throws MalformedURLException, RuntimeException {
        this.numero = numero;
        this.temporada = temporada;

        //Realizando a requisição para a API para descobrir a data e o titulo do episódio
        URL requisicao;
        StringBuilder resultado;
        Matcher regex;
        BufferedReader in;
        try{
            requisicao = new URL("http://api.themoviedb.org/3/tv/" + idSerie + "/season/" + temporada.getNumero() + "/episode/" + numero + "?api_key=f3c5689648013f06492813f7304d91e9"); 
            in = new BufferedReader(new InputStreamReader(requisicao.openStream()));
            resultado = new StringBuilder(in.readLine());
            //Processando a resposta da API com Regex para encontrar a data e o título
            regex = Pattern.compile("(\\d+)-(\\d+)-(\\d+)").matcher(resultado);
            Calendar c = Calendar.getInstance();
            if (regex.find()) {
                c.set(Integer.parseInt(regex.group(1)), Integer.parseInt(regex.group(2)), Integer.parseInt(regex.group(3)));
            } else {
                c.set(0, 0, 0);
            }
            this.data = c.getTime();
            regex = Pattern.compile("(?<=me\":\")(.+?)(?=\",\")").matcher(resultado);
            if (regex.find()) {
                this.titulo = regex.group();
            } else {
                this.titulo = "Não encontrado";
            }
        }catch(IOException e){
            this.titulo = "Epidodio não encontrado";
            this.data = Calendar.getInstance().getTime();
        }

        //Realizando a requisição para a API para descobrir os personagens que participaram desse episódio
        try{
            requisicao = new URL("http://api.themoviedb.org/3/tv/" + idSerie + "/season/" + temporada.getNumero() + "/episode/" + numero + "/credits?api_key=f3c5689648013f06492813f7304d91e9"); 
            in = new BufferedReader(new InputStreamReader(requisicao.openStream()));
            resultado = new StringBuilder(in.readLine());
            
            //Utilizando Regex para encontrar o nome do personagem, o id e o nome do ator e criar os personagens
            regex = Pattern.compile("(?<=r\":[\"|n])(.*?)(?=[l|\"],\").+?(?<=d\":)(\\d+).+?(?<=:\")(.+?)(?=\",\")").matcher(resultado);
            elenco = new ArrayList<>();
            while (regex.find()) {
                int idAtor = Integer.parseInt(regex.group(2));
                Ator atorAtual = null;
                if (Colecoes.findAtor(idAtor) == null) {
                    atorAtual = new Ator(idAtor);
                    Colecoes.addAtor(atorAtual);
                } else {
                    atorAtual = Colecoes.findAtor(idAtor);
                }
                elenco.add(new Personagem((regex.group(1).matches("Herself||Himself||ul||")) ? regex.group(3) : regex.group(1), atorAtual));
            }
            regex = Pattern.compile("(?<=d\":)(\\d+)(?:,\"name\":\")([^\"]*)(?:.{42}character\":[\"|n])(.*?)(?:[l|\"],\")(?!.+stars)(?<=_stars\")(?:.+)(?<=:)(\\d+)(?:,\"name\":\")(.*?)(?=\",\").+?(?<=character\":\")(.*?)(?=\",\")").matcher(resultado);
            while (regex.find()) {
                int idAtor = Integer.parseInt(regex.group(1));
                Ator atorAtual = null;
                if (Colecoes.findAtor(idAtor) == null) {
                    atorAtual = new Ator(idAtor);
                    Colecoes.addAtor(atorAtual);
                } else {
                    atorAtual = Colecoes.findAtor(idAtor);
                }
                Personagem perso = new Personagem(((regex.group(3).toLowerCase()).matches("Herself||Himself||ul||")) ? regex.group(1) : regex.group(2), atorAtual);
                if(elenco.contains(perso)){
                    elenco.add(perso);
                }
         
            }
        }catch(IOException e){
            this.elenco= new ArrayList<>();;
        }
    }

    /**
     * Retorna o número do episódio
     *
     * @return número
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Retorna o título do episódio
     *
     * @return título
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Retorna a data de lançamento do episódio
     *
     * @return dataDeLançamento
     */
    public Date getData() {
        return data;
    }

    /**
     * Retorna um personagem do episódio dado um índice de 0 até,
     * exclusivamente, o valor retornado pelo tamanhoDoElenco()
     *
     * @param indice
     * @return enesimoPersonagem
     */
    public Personagem getPersonagem(int indice) {
        return elenco.get(indice);
    }

    /**
     * Retorna um número de personagens do episódio
     *
     * @return númeroDePersongens
     */
    public int tamanhoDoElenco() {
        return elenco.size();
    }

    @Override
    public String toString() {
        return "T" + ((temporada.getNumero() < 10) ? "0" + temporada.getNumero() : temporada.getNumero()) + "E" + ((numero < 10) ? "0" + numero : numero) + "\tTítulo: " + titulo;
    }
}
