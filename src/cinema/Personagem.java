package cinema;

/**
 * Representa um personagem de uma producao, com seu respectivo nome e ator
 */
public class Personagem {

    private String nome;
    private Ator ator;
    
    /**
     * Cria um personagem
     *
     * @param nomePersonagem
     * @param ator 
     */
    Personagem(String nomePersonagem, Ator ator) {
        if(nomePersonagem.contains("\"")){
            nomePersonagem = nomePersonagem.replaceAll("\"", "");
        }
        this.nome = nomePersonagem;
        this.ator = ator;
    }

    /**
     * Retorna o nome do personagem
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Retorna o ator que representou este personagem
     *
     * @return ator
     */
    public Ator getAtor() {
        return ator;
    }

    /**
     * Compara dois personagens, se forem iguais retorna verdadeiro
     *
     * @param cover
     * @return saoIguais
     */
    @Override
    public boolean equals(Object cover) {
        if (cover instanceof Personagem) {
            return (nome.equals(((Personagem) cover).getNome())) && (ator.equals(((Personagem) cover).getAtor()));
        }
        return false;
    }

    @Override
    public String toString() {
        return ator.getNome()+" como "+ nome;
    }
}
